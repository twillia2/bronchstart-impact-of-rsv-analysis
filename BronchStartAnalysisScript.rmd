---
title: "BronchStart Analysis Script"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE)

#https://benjaminlouis-stat.fr/en/blog/2020-05-21-astuces-ggplot-rmarkdown/
#this makes the pdf graph font size scale right
knitr::opts_chunk$set(
 fig.width = 10,
 fig.asp = 0.8,
 out.width = "100%"
)

require("tidyverse")
require("janitor")
require("gt")
library('gtsummary')
#library('MASS')  is needed but if fully load it breaks select statements
library('glue')
library('ggridges') 

load(file="bronchstart_export_2022-10-12.rdata")

data<-ds%>%
  clean_names()%>%
  mutate(virus = case_when(pcr_res_1==1 ~ 'rsv',
                           pcr_res_2==1 ~ 'other',
                           pcr_res_3==1 ~ 'negative',
                           poct_vir_1==1 ~ 'rsv',
                           poct_vir_2==1 ~ 'other',
                           poct_vir_3==1 ~ 'negative',
                           TRUE ~ 'untested'))%>%
  mutate(virus = as.factor(virus))%>%
  mutate(eligible = case_when(  (elig_age &
                                   (elig_diag_1 | elig_diag_2| elig_diag_3)&
                                   (elig_prev==2 | elig_prev==3)) ~ TRUE,
                                TRUE ~ FALSE))%>%

  mutate(weekNo = isoweek(attend_dat))%>%
  mutate(studyWeek = 1+(dmy("12-04-2021") %--% attend_dat) %/% weeks(1) )%>%
  rename(siteID=site_nam, 
         attendanceDate=attend_dat)%>%
  mutate(pathway_total=pathway_1+pathway_2+pathway_3+pathway_4+pathway_5+pathway_6+pathway_7+pathway_8)%>%
  mutate(pathway_combined=paste(pathway_1,pathway_2,pathway_3,pathway_4,pathway_5,pathway_6,pathway_7,pathway_8))%>%
  mutate(pathway = case_when(pathway_7==1 ~ "Ad/Dc/Ad",
                               pathway_6==1 ~ "Ad/Dc/Dc",
                               pathway_5==1~ "Ad/Dc",
                               pathway_4==1~ "Ad",
                               pathway_3==1~ "Dc/Ad",
                               pathway_2==1~ "Dc/Dc",
                               pathway_1==1~ "Dc",
                               pathway_8==1~ "other"),
           pathway=factor(pathway,levels=c("Dc","Dc/Dc","Dc/Ad","Ad","Ad/Dc","Ad/Dc/Dc","Ad/Dc/Ad","other")))%>%
  #mutate(pathway = as.factor(pathway))%>%
  mutate(pathway_outcome = case_when(pathway_1==1 ~ "discharged",
                            pathway_2==1 ~ "discharged",
                            pathway_3==1~ "admitted",
                            pathway_4==1~ "admitted",
                            pathway_5==1~ "admitted",
                            pathway_6==1~ "admitted",
                            pathway_7==1~ "admitted",
                            pathway_8==1~ "other"))%>%
  mutate(disposition_from_ED = case_when( disposition == 1 ~ 'discharged',
                                       disposition == 2 ~ 'short stay',
                                       disposition == 3 ~ 'admitted',
                                       disposition == 4 ~ 'HDU',
                                       disposition == 5 ~ 'PICU',
                                       disposition == 6 ~ 'died in ED'))%>%
  mutate(follow_up_form_complete=case_when( follow_up_form_complete == 0 ~ 'Incomplete',
                                            follow_up_form_complete == 1 ~ 'Unveried',
                                            follow_up_form_complete == 2 ~ 'Complete'))%>%
  mutate(ed_diag = case_when(   elig_diag_1 == 1 ~ 'Bronchiolitis',
                                elig_diag_2 == 1 ~ 'LRTI',
                                elig_diag_3 == 1 ~ 'FirstWheeze'))%>%
  mutate(sats = as.numeric( str_extract(satn, "[1]?[0-9][0-9]") ))%>%
  mutate(sats_in_air = if_else(satn_air_ox==1,sats,NA_real_))%>%
  mutate(att_num = if_else(att_num==99,NA_real_,att_num))%>%
  mutate(att_num = replace_na(att_num,1))%>%
  mutate(rate_resp = as.numeric( rate_resp))%>%
  mutate(rate_resp = case_when( rate_resp>160 ~ NA_real_,
                                rate_resp<10 ~ NA_real_,
                                TRUE~rate_resp))%>%
  mutate(sex = case_when ( sex ==1~"M",
                           sex ==2~"F"))%>%
  mutate(treat_hosp_any_nutrition=treat_hosp_1|treat_hosp_2)%>%
  mutate(treat_hosp_any_oxygen=treat_hosp_3|treat_hosp_4)%>%
  mutate(highest_level_care = case_when( 
                                         admit_locn_4==1 ~ 'ITU',
                                         admit_locn_3==1 ~ 'HDU',
                                         admit_locn_2==1 ~ 'ward',
                                         admit_locn_1==1 ~ 'observation',
                                         admit_locn_1==0 & admit_locn_2==0 & admit_locn_3==0 & admit_locn_4==0 & admit_locn_5==0 ~ 'ED',
                                         admit_locn_5==1 ~ 'other',
                                         TRUE ~ 'ED'))%>%
    mutate(highest_level_care = case_when(
                                          follow_up_form_complete=='Incomplete' & disposition_from_ED=='PICU' ~ 'ITU',
                                          follow_up_form_complete=='Incomplete' & disposition_from_ED=='HDU' ~ 'HDU',
                                          follow_up_form_complete=='Incomplete' & disposition_from_ED=='admitted' ~ 'ward',
                                          follow_up_form_complete=='Incomplete' & disposition_from_ED=='short stay' ~ 'observation',
                                          follow_up_form_complete=='Incomplete' & disposition_from_ED=='discharged' ~ 'ED',
                                        TRUE ~ highest_level_care)
           )%>%
  mutate(highest_level_care=as.factor(highest_level_care))%>%
  mutate(imd=coalesce(imd_eng,imd_scot))%>%
  mutate(imd=if_else(imd==99,NA_real_,imd))%>%
  #IMD Q1 is poorest, IMD Q5 richest
  mutate(imd_quintile=case_when(imd %in% c(1:2) ~ 'Q1',
                                imd %in% c(3:4) ~ 'Q2',
                                imd %in% c(5:6) ~ 'Q3',
                                imd %in% c(7:8) ~ 'Q4',
                                imd %in% c(9:10) ~ 'Q5'))%>%
  mutate(travel_yn=if_else(travel_yn==2,NA_real_,travel_yn))%>%
  mutate(siblings=if_else(siblings==99,NA_real_,siblings))%>%
  mutate(siblings_yn=if_else(siblings>0,TRUE,FALSE))%>%
  mutate(daycare=case_when(daycare==3 ~ NA,
                          daycare==1 ~ TRUE,
                          daycare==2 ~ FALSE))%>%
  mutate(age_grp = cut(age_mnths, breaks=c(-1,1,3,6,9,12,25),
                        labels=c("<1m","1-<3m","3-<6m","6-<9m","9-11m","1-2y"),right=FALSE))%>%
  mutate(age_wks_new=coalesce(age_wks, age_mnths*4 ))%>%
  mutate(admitted = if_else(highest_level_care == 'ED','not admitted','admitted'))%>%
  mutate(admitted = as.factor(admitted))%>%
  mutate(admittedTF = if_else(highest_level_care == 'ED',FALSE,TRUE))%>%
  mutate(still_admitted_d7=case_when(outcome_location=='1' ~ FALSE,
                                     outcome_location=='2' ~ TRUE,
                                     outcome_location=='3' ~ TRUE,
                                     outcome_location=='4' ~ TRUE))%>%
  mutate(premature = if_else(comorb_1	| comorb_2, TRUE, FALSE))%>%
  mutate(monthno=interval(ymd("2021-04-12"),ymd(attendanceDate))%/% months(1))%>%
  mutate(travel_dest = case_when(travel_region_7==1 ~ "international",
                                 travel_region_6==1 ~ "europe",
                                 travel_region_5==1 ~ "UK",
                                 travel_region_4==1 ~ "UK",
                                 travel_region_3==1 ~ "UK",
                                 travel_region_2==1 ~ "UK",
                                 travel_region_1==1 ~ "UK"))%>%
  mutate(gas_ph_num = if_else((gas_ph_num>6.7 & gas_ph_num<7.6),gas_ph_num,NA_real_))%>%
  mutate(gas_paco2_num = if_else((gas_paco2_num>3.4 & gas_paco2_num<8.6),gas_paco2_num,NA_real_))%>%
  mutate(comorb_ranked = case_when(comorb_3==1 & comorb_1==1 ~ 'prem+CHD',
                                   comorb_2==1 ~ 'other', #CLD
                                   comorb_4==1 ~ 'other', #neuromuscular
                                   comorb_5==1 ~ 'other', #other
                                   comorb_3==1 ~ 'congenital heart disease',
                                   comorb_1==1 ~ 'prem',
                                   comorb_6==1 ~ 'none',
                                   TRUE~'none'))

size_whole<-nrow(data)                                

size_daterange<-data%>%
  filter(attendanceDate %within% interval(dmy('30/04/2021'),dmy('01/05/2022')))%>%
  nrow()

data<-
  data%>%
  filter(age_mnths<24)%>%
  filter(emergency_department_attendance_complete==2)%>%
  drop_na(siteID)%>%
  filter(attendanceDate %within% interval(dmy('01/05/2021'),dmy('30/04/2022')))

data<-
data%>%
  mutate(virus=fct_relevel(virus, c("rsv","other","negative","untested")))%>%
  mutate(highest_level_care=fct_relevel(highest_level_care, c("ED","observation","ward","HDU","ITU")))
  
```

## Case recruitment to the BronchStart study 
Of whole dataset `r size_whole` 
Analysis window set to period `r min(data$attendanceDate)` to `r max(data$attendanceDate)` 
This gives `r size_daterange` attendances and `r nrow(data)` once cleaned.

```{r t1_inline_data}

ed_diag<-data%>%
  tabyl(ed_diag)%>%
  mutate(total=nrow(data))%>%
  mutate(percent=round(percent * 100,1))%>%
  mutate(sum=glue("({n} / {total} {percent}%)"))

admit<-data%>%
  tabyl(admittedTF)%>%
  #filter(admittedTF=='FALSE')%>%
  mutate(percent=round(percent * 100,1))%>%
  mutate(sum=glue("({n} {percent}%)"))

outcome<-data%>%
  tabyl(highest_level_care)%>%
  #filter(admittedTF=='FALSE')%>%
  mutate(percent=round(percent * 100,1))%>%
  mutate(sum=glue("{n} ({percent}%)"))

Scot_site_id<-c(44,51,52,89)

Ire_site_id<-c(10,11)

countries<-data%>%
  mutate( country = case_when( siteID %in% Scot_site_id ~ "Scotland",
                               siteID %in% Ire_site_id ~ "Ireland",
                               TRUE ~ "England"))%>%
  tabyl(country)%>%
  mutate(percent=round(percent * 100,1))%>%
  mutate(sum=glue("{n} ({percent}%)"))

vpos<-data%>%
  filter(virus!='untested')%>%
  tabyl(virus)%>%
  mutate(percent=round(percent * 100,1))

data%>%
  mutate( country = case_when( siteID %in% Scot_site_id ~ "Scotland",
                               siteID %in% Ire_site_id ~ "Ireland",
                               TRUE ~ "England"))%>%
  tabyl(country,ed_diag)%>%
  adorn_totals("col")%>%
  mutate(across(Bronchiolitis:LRTI,~round((./Total)*100,1), .names="{.col}_p"))%>%
  mutate(across(Bronchiolitis:LRTI,~ paste0( .x, " (",
                  eval(parse(text=paste0(expr(.x),"_p"))),
                  "%)")
                ))%>%
  select(country:Total)%>%
  gt()

data%>%
  filter(elig_prev==3)%>%
  tabyl(sex)%>%
  gt()%>%
  tab_header("Gender")

data%>%
  filter(elig_prev==3)%>%
  tabyl(imd_quintile)%>%
  gt()%>%
  fmt_percent(columns = 3:4,decimals = 1)%>%
  tab_header("IMD quintile (1 poorest, 5 richest")
         
```

In the period `r min(data$attendanceDate)` to `r max(data$attendanceDate)`, `r size_daterange` cases were recruited to the BronchStart study. 

Of these, `r nrow(data)` cases with a complete dataset were submitted for `r data%>% filter(elig_prev==3)%>% nrow()` individual children from `r distinct(data,siteID)%>%nrow()`sites across England, Scotland and the Republic of Ireland with a clinician assigned diagnosis of bronchiolitis, lower respiratory tract infection or first episode of wheeze (Figure 1, panel A). 

Of the cases, `r countries[1,][4]` were recruited at English sites, `r countries[3,][4]` in Scotland and `r countries[2,][4]` in the Republic of Ireland (Supplementary Figure 1). 

Males predominated (`r data%>% filter(sex=="M"& elig_prev==3)%>% nrow()`/`r data%>% filter(elig_prev==3)%>% nrow()`; 60.4%), and of 16,443 children with socio-economic status available, `r data%>% filter(imd_quintile=="Q5" & elig_prev==3)%>% nrow()` (XX%) were from the least deprived quintile, and `r data%>% filter(imd_quintile=="Q1" & elig_prev==3)%>% nrow()` (XX%) from the most deprived quintile.   

Testing was most common for infants and children admitted to hospital, where RSV was the most common single pathogen identified; overall, `r vpos[1,][3]`% of those with a positive test result tested positive for RSV. 


## Highest Level of Care
The most common diagnosis assigned to the BronchStart cohort was bronchiolitis `r ed_diag[ed_diag=='Bronchiolitis',]$sum`, followed by first episode of viral induced wheeze `r  ed_diag[ed_diag=='FirstWheeze',]$sum` and lower respiratory tract infection `r ed_diag[ed_diag=='LRTI',]$sum` (Table 1). The majority of cases `r admit[1,]$sum` were assessed in the Emergency Department and discharged home (Table 1). 

A total of `r admit[2,]$sum` cases from the cohort were admitted to hospital; `r data%>%filter(disposition==6)%>%nrow()` patients died in the Emergency Department. 

The most common admission location was a hospital ward `r outcome[3,]$sum` or an observation unit `r outcome[2,]$sum`. Of the cohort, `r outcome[4,]$sum` required high dependency level care, and a further `r outcome[5,]$sum` were admitted to a paediatric intensive care unit. 

## Table 1
```{r table1}
data%>%
  select(ed_diag,virus,highest_level_care)%>%
  rename('diagnosis'='ed_diag','Location'=highest_level_care)%>%
  tbl_summary(by=Location)%>%
  modify_caption("**Table 1: Level of care provided to BronchStart cases**")

#data%>%
#  tabyl(highest_level_care,admittedTF)

#data%>%
#  filter(highest_level_care=='other' & admittedTF)


```

## Pathway Analysis
```{r table1_pathway}

data%>%tabyl(elig_prev)%>%
  mutate(elig_prev=case_when(elig_prev==3 ~ "new recruit",
                             elig_prev==2 ~ "previously recruited"))%>%
  gt()%>%
  fmt_percent(columns = percent,decimals = 1)%>%
  tab_header("Number previously recruited")

data%>%
  tabyl(pathway)%>%
  select(-valid_percent)%>%
  gt()%>%tab_header(title = md("Pathyway follow up at 7d"))%>%
  fmt_percent(columns = percent,decimals = 1)
```

\newpage
## Figure 1
### Virus detection over time
```{r f1b}
data%>%
  tabyl(weekNo,virus)%>%
  mutate(weekOrder = (c(37:52,1:36)))%>%
  arrange(weekOrder)%>%
  mutate(weekNo=forcats::as_factor(as.character(weekNo)))%>%
  select(-weekOrder)%>%
  pivot_longer(!weekNo,names_to = "virus",values_to = "count")%>%
  mutate(virus=factor(virus, levels=c("untested","negative","other","rsv")))%>%
  ggplot(aes(x=weekNo, y=count, fill=virus)) +
  geom_area(aes(colour = virus, group=virus, fill = virus))+
  labs(x = "epidemiological week", y='Count')+
  scale_x_discrete("epidemiological week", breaks=c(seq(17,52,2),seq(1,16,2)),labels=c(seq(17,52,2),seq(1,16,2)))+
  scale_fill_brewer(palette = "Set2")

ggsave("BronchStart_rsv_other.pdf",bg="white")

```

### ridgeplot of other viruses
```{r}

data%>%
  mutate(v_rsv =if_else(poct_vir_1==1 | pcr_res_1==1,TRUE,FALSE),
         #v_other =if_else(poct_vir_2==1 | pcr_res_2==1,TRUE,FALSE),
         #v_neg =if_else(poct_vir_3==1 | pcr_res_3==1,TRUE,FALSE),
         
         v_adeno =if_else(poct_vir_othr_1==1 | pcr_vir_othr_1==1,TRUE,FALSE),
         v_hmpv  =if_else(poct_vir_othr_2==1 | pcr_vir_othr_2==1,TRUE,FALSE),
         v_flu   =if_else(poct_vir_othr_3==1 | pcr_vir_othr_3==1,TRUE,FALSE),
         v_pflu  =if_else(poct_vir_othr_4==1 | pcr_vir_othr_4==1,TRUE,FALSE),
         v_rhino =if_else(poct_vir_othr_5==1 | pcr_vir_othr_5==1,TRUE,FALSE),
         v_covid =if_else(poct_vir_othr_6==1 | pcr_vir_othr_6==1,TRUE,FALSE),
         #v_other =if_else(poct_vir_othr_7==1 | pcr_vir_othr_7==1,TRUE,FALSE),
         )%>%
  select(studyWeek, starts_with('v_'))%>%
  group_by(studyWeek)%>%
  summarise(across(starts_with('v_'), sum))%>%
  mutate(weekOrder = (c(37:53,1:36)))%>%
  arrange(weekOrder)%>%
  mutate(weekOrder = (c(1:53)))%>%
  select(-weekOrder)%>%
  rename("rsv"=v_rsv,
         "rhinovirus"=v_rhino,
         #"no virus detected"=v_neg,
         "influenza"=v_flu,
         "parainfluenza"=v_pflu,
         "COVID19"=v_covid,
         #"other virus"=v_other,
         "adenovirus"=v_adeno,
         "humanmetapneumovirus"=v_hmpv
         )%>%
  pivot_longer(!studyWeek,names_to = "virus",values_to = "count")%>%
  mutate(height=case_when(virus=='rsv' ~ 70,
                          virus=='rhinovirus' ~ 60,
                          virus=='influenza'  ~ 50,
                          virus=='parainfluenza'  ~ 40,
                          virus=='COVID19'  ~ 30,
                          virus=='adenovirus'  ~ 20,
                          virus=='humanmetapneumovirus'  ~ 10))%>%
  ggplot(aes(x=studyWeek, y=height, height=count, fill=virus)) +
  geom_ridgeline()+
  theme_ridges()+
  scale_x_continuous("epidemiological week", breaks=seq(2,53,2),labels=c(seq(17,51,2),seq(1,16,2)))+
  theme(legend.position = "bottom")+
  #guides(fill = guide_legend(nrow = 1))+
  theme(axis.text.y=element_blank(),
        axis.ticks.y=element_blank() 
        )+
  ylab(NULL)

ggsave("BronchStart_ridgeplot_v2.pdf",bg="white")
ggsave("BronchStart_ridgeplot_v2.png",bg="white")
```

```{r t2_inline_data}

LF<-data%>%filter(admittedTF)%>%tabyl(ed_diag,treat_hosp_3)%>%
  rowwise()%>%
  mutate(total=sum(`0`+`1`),
         percent=round(`1`/total*100,1),
         sum=glue("({`1`}/{total} {percent}%)"))

HF<-data%>%filter(admittedTF)%>%tabyl(ed_diag,treat_hosp_4)%>%
  rowwise()%>%
  mutate(total=sum(`0`+`1`),
         percent=round(`1`/total*100,1),
         sum=glue("({`1`}/{total} {percent}%)"))

abx<-data%>%filter(admittedTF)%>%tabyl(ed_diag,treat_pharm_1)%>%
  rowwise()%>%
  mutate(total=sum(`0`+`1`),
         percent=round(`1`/total*100,1),
         sum=glue("({`1`}/{total} {percent}%)"))


```

\newpage
## Table 2
For the admitted cohort, low flow oxygen therapy was administered commonly for those with a diagnosis of bronchiolitis `r LF[1,][6]`, first wheeze `r LF[2,][6]` or LRTI `r LF[3,][6]` (Table 2). 

High flow oxygen was administered most commonly for those with a diagnosis of bronchiolitis `r HF[1,][6]`, followed by those with LRTI `r HF[3,][6]`, and less commonly for those with a diagnosis of first episode of wheeze `r HF[2,][6]`. 

Antibiotics were administered to over two thirds of those with a diagnosis of lower respiratory tract infection `r abx[3,][6]` and to over a fifth of those with  a diagnosis of bronchiolitis `r abx[1,][6]`. 


```{r table2}
data%>%
   filter(admitted == "admitted")%>%
  select(ed_diag,starts_with("treat_hosp"), starts_with("treat_pharm"),-c("treat_pharm_bronch","treat_pharm_othr"))%>%
    rename("NG fluids"=treat_hosp_1,
         "IV fluids"=treat_hosp_2,
         "fluids_any"=treat_hosp_any_nutrition,
         "Oxygen_any"=treat_hosp_any_oxygen,
         "Oxygen_LF"=treat_hosp_3,
         "Oxygen_HF"=treat_hosp_4,
         "CPAP/BIPAP"=treat_hosp_5,
         "IMV"=treat_hosp_6,
         "pharmacological"=treat_hosp_7,
         "none"=treat_hosp_8)%>%
   rename("Antibiotics"=treat_pharm_1,
          "Caffeine"=treat_pharm_2,
          "MgSo4"=treat_pharm_3,
          "Prednisolone"=treat_pharm_4,
          "Salbutamol inh"=treat_pharm_5,
          "Salbutamol IV"=treat_pharm_6,
          "Other Bronchodilator"=treat_pharm_7,
          "Other"=treat_pharm_8)%>%
  tbl_summary( by=ed_diag,
    statistic = list(all_continuous() ~ "{median} ({p25}, {p75}) / {mean} ({sd}) / {min}-{max}")
  )%>%
  #add_p()%>%
  modify_caption("**Admitted Bronchstart patients therapies by diagnosis**")
```

\newpage
## Table 3 Comorbidities vs level of care
#### one comorbidity only  (ranked CHD+PREM, CHD only, Prem only, anything else as other, none)
```{r}

data%>%
  mutate(comorb_ranked=factor(comorb_ranked, levels=c("prem+CHD","congenital heart disease","prem","other","none")))%>%
  tabyl(highest_level_care,comorb_ranked)%>%
  adorn_totals("col")%>%
  adorn_percentages("row") %>%
  adorn_pct_formatting(digits = 1) %>%
  adorn_ns( position = "front")%>%
  gt()%>%
  tab_header(title = md("Highest level of care by single ordered comorbidity"))


```

#### T3 all single comorbidities (for interest)
```{r}

data%>%
  group_by(highest_level_care)%>%
  summarise(prem=sum(comorb_1),
            CLD=sum(comorb_2),
            CHD=sum(comorb_3),
            NMD=sum(comorb_4),
            other=sum(comorb_5),
            none=sum(comorb_6),
            total=n()
            )%>%
  ungroup()%>%
  adorn_totals()%>%
  #create new percentage columns _p
  mutate(across(prem:none,~round((./total)*100,1), .names="{.col}_p"))%>%
  # this is magic,  takes column name (as .x), & creates '.x_p' then converts that into a variable so can add %
  mutate(across(prem:none,~ paste0( .x, " (",
                  eval(parse(text=paste0(expr(.x),"_p"))),
                  "%)")
                ))%>%
  dplyr::select(highest_level_care:total)%>%
  gt()%>%
  tab_header(title = md("categories of comorbidities that patients admitted have"),
             subtitle = "n.b. patients can have more than one comorbidity")

```

\newpage
## Sup table 2: Proportional odds model analysis for reviewer 1
```{r proportional_odds}
#https://peopleanalytics-regression-book.org/ord-reg.html

regression_data<-data%>%
  mutate(admit_PICU = if_else(admit_locn_4==1,TRUE,FALSE))%>%
  mutate(admit_HDU = if_else(admit_locn_3==1,TRUE,FALSE))%>%
  mutate(comorbidity = case_when(comorb_6==1 ~ "no cormorbidities",
                                 comorb_1==1 ~ "premature"))%>%
  mutate(comorbidity= fct_relevel(comorbidity,c('no cormorbidities','premature')))%>%
  drop_na(comorbidity)%>%
  mutate(rsv = if_else(virus=='rsv',TRUE,FALSE))%>%
    mutate(age_grp = cut(floor(age_mnths), breaks=c(-1,3,5,8,11,25),
                        labels=c("0-2m","3-5m","6-8m","9-11m","12-24m"),right=FALSE))%>%
    mutate(age_gest_grp=case_when(
    age_gest<7 ~ '<28w',
    age_gest<11 ~ '28-31w',
    age_gest<16 ~ '32-36w',
    #age_gest==99 ~ 'N/K',
    TRUE~'term'))%>%
  mutate(age_gest_grp=factor(age_gest_grp,levels = c('term','<28w','28-31w','32-36w')))%>%
  mutate(age_grp=factor(age_grp,levels = c("12-24m","0-2m","3-5m","6-8m","9-11m")))%>%
  select(comorbidity,admittedTF,treat_hosp_3,treat_hosp_4,treat_hosp_5,treat_hosp_6,admit_HDU,admit_PICU,rsv,age_gest_grp,age_grp,ed_diag)%>%
  rename("Oxygen_LF"=treat_hosp_3,
         "Oxygen_HF"=treat_hosp_4,
         "CPAP_BIPAP"=treat_hosp_5,
         "IMV"=treat_hosp_6)

regression_data<-
regression_data%>%
  mutate(level_of_care = case_when(admit_PICU ~ "picu",
                                   admit_HDU ~  "hdu",
                                   admittedTF ~ "ward",
                                   TRUE ~ "home"))%>%
  mutate(level_of_care = ordered(level_of_care , levels = c("home","ward", "hdu", "picu")))%>%
  mutate(level_of_support = case_when(IMV == 1 ~"imv",
                                   CPAP_BIPAP ==1 ~  "cpap",
                                   Oxygen_HF ==1 ~ "hfnc",
                                   Oxygen_LF ==1 ~ "lfo",
                                   TRUE ~ "nil"))%>%
  mutate(level_of_support = ordered(level_of_care , levels = c("nil","lfo", "hfnc", "cpap","imv")))
 
MASS::polr(
  formula = level_of_care ~ comorbidity, 
  data = regression_data)%>%
  broom::tidy(exponentiate=TRUE, conf.int=TRUE)%>%
  filter(coef.type=='coefficient')%>%
  select(term,estimate,conf.low,conf.high)%>%
  rename(OR=estimate)%>%
  gt()%>%
    fmt_number(columns = 2:4, decimals = 1)%>%
    tab_header("Odds of admission (any level) ~ premature / no comorbidities")

MASS::polr(
  formula = level_of_care ~ age_gest_grp + age_grp, 
  data = regression_data)%>%
  broom::tidy(exponentiate=TRUE, conf.int=TRUE)%>%
  filter(coef.type=='coefficient')%>%
  mutate(result=sprintf("%1.2f (%1.2f-%1.2f)",estimate,conf.low,conf.high))%>%
  select(term,result)%>%
  gt()%>%
    cols_label(result = "OR (95% CI)")%>%
    tab_header("Proportional odds: Overall risk of admission adjusted for gestation and age")%>%
    tab_footnote("formula = level_of_care ~ age_gest_grp + age_grp\n reference term and age 12-24m")

glm(
  formula = admittedTF ~ age_gest_grp + age_grp, 
  data = regression_data,
  family=binomial)%>%
  broom::tidy(exponentiate=TRUE, conf.int=TRUE)%>%
  filter(term!='(Intercept)')%>%
  mutate(result=sprintf("%1.2f (%1.2f-%1.2f)",estimate,conf.low,conf.high))%>%
  dplyr::select(term,result)%>%
  gt()%>%
    cols_label(result = "OR (95% CI)")%>%
    tab_header("Logistic regression: Admission TF adjusted for gestation and age")%>%
    tab_footnote("formula = admissionTF ~ age_gest_grp + age_grp\n reference term and age 12-24m")


MASS::polr(
  formula = level_of_care ~ age_gest_grp + age_grp, 
  data = regression_data%>%filter(ed_diag=="Bronchiolitis"))%>%
  broom::tidy(exponentiate=TRUE, conf.int=TRUE)%>%
  filter(coef.type=='coefficient')%>%
  mutate(result=sprintf("%1.2f (%1.2f-%1.2f)",estimate,conf.low,conf.high))%>%
  select(term,result)%>%
  gt()%>%
    cols_label(result = "OR (95% CI)")%>%
    tab_header("Proportional odds: but using only using those diagnosed as bronchiolitis")%>%
    tab_footnote("formula = level_of_care ~ age_gest_grp + age_grp\n reference term and age 12-24m")
```

\newpage
## Table 4
Of the BronchStart cohort, `r data%>%filter(!comorb_6==1)%>%nrow()` (`r round(data%>%filter(!comorb_6==1)%>%nrow()/nrow(data)*100,1)`%) were classified as having one or more co-morbidities, including preterm birth. Of the cohort, `r data%>%filter(comorb_1==1)%>%nrow()` (`r round(data%>%filter(comorb_1==1)%>%nrow()/nrow(data)*100,1)`%) were born preterm, `r data%>%filter(comorb_2==1)%>%nrow()` `r round(data%>%filter(comorb_2==1)%>%nrow()/nrow(data)*100,1)`% had chronic lung disease of prematurity `r data%>%filter(comorb_3==1)%>%nrow()` `r round(data%>%filter(comorb_3==1)%>%nrow()/nrow(data)*100,1)`% had congenital cardiac disease. `r data%>%filter(comorb_4==1)%>%nrow()` (`r round(data%>%filter(comorb_4==1)%>%nrow()/nrow(data)*100,1)`%) had neurological disorder.
Examining this group, those identified as being preterm were more likely to be admitted, receive oxygen, receive high-flow oxygen therapy, be admitted to HDU/PICU, or receive invasive mechanical ventilation (Table 3). 

The same increased risk was found for those children with congenital cardiac disease.


```{r t3_prems}

inlineRR <- function(data, variable, by, ...) {

  f<-as.formula(paste0(substitute(variable),' ~ comorbidity'))
  
  glm(data=data, f, family=binomial(link='log'))%>%
    broom::tidy(exponentiate=TRUE, conf.int=TRUE)%>%
    filter(term!='(Intercept)')%>%
    rename(RR=estimate)%>%
    mutate(`95% CI`=paste0("(",round(conf.low,2)," - ",round(conf.high,2),")"))%>%
    #select(estimate,conf.low,conf.high,p.value)
    select(RR,`95% CI`,p.value)
}

data%>%
  mutate(admit_PICU = if_else(admit_locn_4==1,TRUE,FALSE))%>%
  mutate(admit_HDU = if_else(admit_locn_3==1,TRUE,FALSE))%>%
  mutate(comorbidity = case_when(comorb_6==1 ~ "no cormorbidities",
                                 comorb_1==1 ~ "premature"))%>%
  mutate(comorbidity= fct_relevel(comorbidity,c('no cormorbidities','premature')))%>%
  drop_na(comorbidity)%>%
  select(comorbidity,admittedTF,treat_hosp_3,treat_hosp_4,treat_hosp_5,treat_hosp_6,admit_HDU,admit_PICU)%>%
  rename("Oxygen_LF"=treat_hosp_3,
         "Oxygen_HF"=treat_hosp_4,
         "CPAP_BIPAP"=treat_hosp_5,
         "IMV"=treat_hosp_6)%>%
  tbl_summary( by=comorbidity)%>%
  add_stat(fns = everything() ~ inlineRR) %>%
  #add_p()%>%
  modify_caption("**BronchStart treatments for prematurity vs no comorbidities**")
```
(NB these are those with prem + other comorbities too rather than just prems)

## Supplementary table 3
```{r t3_CHD}
data%>%
  mutate(admit_PICU = if_else(admit_locn_4==1,TRUE,FALSE))%>%
  mutate(admit_HDU = if_else(admit_locn_3==1,TRUE,FALSE))%>%
  mutate(comorbidity = case_when(comorb_6==1 ~ 'no cormorbidities',
                                 comorb_3==1 ~ 'CHD'))%>%
  mutate(comorbidity= fct_relevel(comorbidity,c('no cormorbidities','CHD')))%>%
  drop_na(comorbidity)%>%
  select(comorbidity,admittedTF,treat_hosp_3,treat_hosp_4,treat_hosp_5,treat_hosp_6,admit_HDU,admit_PICU)%>%
  rename("Oxygen_LF"=treat_hosp_3,
         "Oxygen_HF"=treat_hosp_4,
         "CPAP_BIPAP"=treat_hosp_5,
         "IMV"=treat_hosp_6)%>%
  tbl_summary( by=comorbidity,
    #statistic = list(all_continuous() ~ "{median} ({p25}, {p75}) / {mean} ({sd}) / {min}-{max}")
  )%>%
  #add_p()%>%
  add_stat(fns = everything() ~ inlineRR) %>%
  modify_caption("**BronchStart treatments for CHD vs no cormorbidities**")

```
(NB currently these are those with CHD + other comorbities too rather than just CHD)


### Data analysis
Data extraction and analysis was done using R version 4.2.2.(1)  Descriptive tables were generated using gtsummary(2), Risk ratios and confidence intervals were calculated using log binomial regression models.  Analysis scripts are available at (can I make a doi and upload them somewhere or put as a supplement)

1) R Core Team (2022). R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria.   URL https://www.R-project.org/.

2) Sjoberg DD, Whiting K, Curry M, Lavery JA, Larmarange J. Reproducible summary tables with the gtsummary package. The R Journal 2021;13:570–80. https://doi.org/10.32614/RJ-2021-053.